import config from 'config';

const twilioConfig = config.get('twilio');

export const twilioSendBody = {
  from: twilioConfig.outgoing
};