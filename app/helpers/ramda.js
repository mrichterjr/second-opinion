import * as R from 'ramda';

export const merge = (obj1, obj2) => R.mergeDeepRight(obj1, obj2);

export const isIn = (str, obj) => R.includes(str, obj);