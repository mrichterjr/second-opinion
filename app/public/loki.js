import loki from 'lokijs';

const db = new loki('sandbox.db');

export const messages = db.addCollection('messages');
