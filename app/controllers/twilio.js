import express from 'express';
import { messages } from '../public/loki';
import { merge } from '../helpers/ramda';
import { twilioSendBody } from '../helpers/twilioMessages';
import twilio from 'twilio';
import config from 'config';
import * as R from 'ramda';

const accountSid = config.get('twilio.accountSid');
const authToken = config.get('twilio.authToken');
const client = twilio(accountSid, authToken);

const app = express();

const opinions = {
  like: 0,
  dislike: 0,
  love: 0
};

app.post('/', async (req, res) => {
  req.body.memberId = req.body.memberId.toString();
  req.body.styleId = req.body.styleId.toString();
  const body = req.body;
  const previousMessage = messages.findOne({
    memberId: req.body.memberId,
    styleId: req.body.styleId
  });
  if (R.isNil(previousMessage)) {
    const body = merge(req.body, opinions);
    messages.insert(body);
  } else {
    previousMessage.memberId = body.memberId;
    previousMessage.styleId = body.styleId;
    previousMessage.phoneNumbers.push(...req.body.phoneNumbers);
    messages.update({ ...previousMessage, imageUrl: body.imageUrl });
  }
  body.phoneNumbers.forEach(number => {
    twilioSendBody.mediaUrl = body.imageUrl;
    twilioSendBody.body = body.copy;
    twilioSendBody.to = number;
    client.messages.create(twilioSendBody);
  });

  return res.status(200).send({
    success: true,
    results: {
      smsSent: body.phoneNumbers.length
    },
    errors: []
  });
});


const findMaxCreatedOrRevised = arr => {
  const created =  Math.max.apply(null, arr.map(obj => obj.meta.created));
  const revision =  Math.max.apply(null, arr.map(obj => obj.meta.revision));
  return (created > revision ? arr.filter(obj =>  obj.meta.created === created) : arr.filter(obj => obj.meta.revision === revision))[0];
};

const opinionMap = ['like', 'dislike', 'love'];


app.post('/receive', (req) => {
  const receivedNumber = req.body.From;
  const body = parseInt(req.body.Body.trim());

  const allSent = messages.find({ phoneNumbers: { '$contains': receivedNumber } });
  const lastSent = findMaxCreatedOrRevised(allSent);
  lastSent[opinionMap[body-1]] = lastSent[opinionMap[body-1]] + 1;
  console.log('received');
});

app.get('/opinions',  (req, res) => {
  const memberId = req.query.memberId;
  const body = messages.find({ memberId });
  return res.status(200).send(body);
});

app.get('/opinion', (req, res) => {
  const memberId = req.query.memberId;
  const styleId = req.query.styleId;
  const body = messages.find({ styleId, memberId});
  return res.status(200).send(body);
});

module.exports = app;