import express from 'express';
import twilio from './twilio';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();

app.use(cors());

app.listen(3400,()=>
  console.log(`Server is listening on port 3400`));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/twilio', twilio);

module.exports = app;
